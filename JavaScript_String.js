function splitString(str){
	let resultStr = str.split(" ");
	return resultStr;
}

function extractString(str,startIndex,len){
	let resultStr = str.substr(startIndex,len);
	return resultStr;
}

function changeCaseString(str){
	let strLen=str.length;
	let newStr='';
	for (let i=0;i<strLen;i++){
		if (str.charCodeAt(i)>64 && str.charCodeAt(i) < 94){
			newStr =newStr + String.fromCharCode(str.charCodeAt(i)+32);	
		}
		else if(str.charCodeAt(i) > 94 && str.charCodeAt(i) < 123){
			newStr =newStr + String.fromCharCode(str.charCodeAt(i)-32);
		}
		else{
			newStr = newStr + str.charAt(i);
		}
		
	}
	return newStr;
}
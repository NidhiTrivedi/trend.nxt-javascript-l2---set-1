
//Assignment 2
function randomInt(){
	return Math.random(); 
}

//Assignment 3
function (n,d){
	var nsign = n >= 0 ? 1 : -1;
  return (Math.round((n*Math.pow(10,d))+(nsign*0.0001))/Math.pow(10,d)).toFixed(d);
}